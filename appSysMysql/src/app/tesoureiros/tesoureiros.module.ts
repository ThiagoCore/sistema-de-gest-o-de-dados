import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TesoureirosPageRoutingModule } from './tesoureiros-routing.module';

import { TesoureirosPage } from './tesoureiros.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TesoureirosPageRoutingModule
  ],
  declarations: [TesoureirosPage]
})
export class TesoureirosPageModule {}
