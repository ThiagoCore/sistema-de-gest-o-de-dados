import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TesoureirosPage } from './tesoureiros.page';

describe('TesoureirosPage', () => {
  let component: TesoureirosPage;
  let fixture: ComponentFixture<TesoureirosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TesoureirosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TesoureirosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
