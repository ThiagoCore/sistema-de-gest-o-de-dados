import { ToastController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Post } from 'src/services/post';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tesoureiros',
  templateUrl: './tesoureiros.page.html',
  styleUrls: ['./tesoureiros.page.scss'],
})
export class TesoureirosPage implements OnInit {

  nome : string;
  usuario : string;
  nivel : string;

  dadosLogin: any;
  entradas: string;
  saidas: string;
  total: number;

  constructor(private actRouter: ActivatedRoute, private router: Router, private provider: Post, private storage: NativeStorage, public toast: ToastController) { }

  ngOnInit() {
   
  }

  logout(){
    this.storage.clear();
    this.router.navigate(['/login']);
  }


  ionViewWillEnter(){
     this.storage.getItem('session_storage').then((res)=>{
      this.dadosLogin = res;
      this.nome = this.dadosLogin.nome;
      this.nivel = this.dadosLogin.nivel;
      this.usuario = this.dadosLogin.usuario;
    });
   this.carregar();
  }


  carregar(){
    return new Promise(resolve => {
      
      let dados = {
        requisicao : 'recuperar',
         
         };

        this.provider.dadosApi(dados, 'apiFinanceiro.php').subscribe(data => {

          if(data['success']) {
            this.entradas = data['result']['entradas'];
            this.saidas = data['result']['saidas'];
            this.total = data['result']['total'];
            
          }
          
        
    });
  });

}

}
