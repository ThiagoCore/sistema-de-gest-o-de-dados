import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Post } from 'src/services/post';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.page.html',
  styleUrls: ['./medicos.page.scss'],
})
export class MedicosPage implements OnInit {

  nome : string;
  usuario : string;
  nivel : string;


  total_pendentes: string;
  total_hoje: string;
  total_aguardando: string;

  dadosLogin: any;

  constructor(private actRouter: ActivatedRoute, private router: Router, private provider: Post, private storage: NativeStorage, public toast: ToastController) { }

  ngOnInit() {
   /* this.actRouter.params.subscribe((data:any)=>{
      
      this.nome = data.nome;
      this.usuario = data.usuario;
      
      this.nivel = data.nivel;

    });*/

    this.storage.getItem('session_storage').then((res)=>{
      this.dadosLogin = res;
      this.nome = this.dadosLogin.nome;
      this.nivel = this.dadosLogin.nivel;
      this.usuario = this.dadosLogin.usuario;
    });

    this.carregar();
  }

  logout(){
    this.storage.clear();
    this.router.navigate(['/login']);
  }


  ionViewWillEnter(){
      this.carregar();
  }

  carregar(){
    return new Promise(resolve => {
      
      let dados = {
        requisicao : 'recuperar',
        usuario : this.usuario, 
         };

        this.provider.dadosApi(dados, 'apiMedicos.php').subscribe(data => {

          if(data['success']) {
            this.total_pendentes = data['result']['pendentes'];
            this.total_hoje = data['result']['hoje'];
            this.total_aguardando = data['result']['aguardando'];
          }
          
        
    });
  });

}


consultas(usuario){
  this.router.navigate(['/consultas/' + usuario]);
}


}