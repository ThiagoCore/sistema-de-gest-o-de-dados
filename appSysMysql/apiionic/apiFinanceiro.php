<?php

include_once('conexao.php');

$postjson = json_decode(file_get_contents('php://input'), true);


 if($postjson['requisicao'] == 'recuperar'){

    
  $res = $pdo->query("SELECT * from movimentacoes where data = curDate()");
  $dados = $res->fetchAll(PDO::FETCH_ASSOC);
  
  $total_entradas = 0;
  $total_saidas = 0;
  
  for ($i=0; $i < count($dados); $i++) { 
    foreach ($dados[$i] as $key => $value) {
    }
  
    $id = $dados[$i]['id'];	
    $tipo = $dados[$i]['tipo'];
    $valor = $dados[$i]['valor'];
    
  
    if($tipo == 'Entrada'){
      @$total_entradas = $total_entradas + $valor;
    }else{
      @$total_saidas = $total_saidas + $valor;
    }
  
  }
  
  @$total = @$total_entradas - @$total_saidas;


$dados = array(
  'entradas' => $total_entradas,
  'saidas' => $total_saidas,
 'total' => $total,
          
);
   

        if($dados){
                $result = json_encode(array('success'=>true, 'result'=>$dados));

            }else{
                $result = json_encode(array('success'=>false, 'result'=>'0'));

            }
            echo $result;

}



else if($postjson['requisicao'] == 'listar'){


 
  if($postjson['dataBuscar'] == ''){
      $query = $pdo->query("SELECT * from contas_pagar where vencimento = curDate() order by id asc limit $postjson[start], $postjson[limit]");
  }else{
    $busca = $postjson['dataBuscar'];
    $query = $pdo->query("SELECT * from contas_pagar where vencimento = '$busca' order by id asc limit $postjson[start], $postjson[limit]");
  }


  $res = $query->fetchAll(PDO::FETCH_ASSOC);

 for ($i=0; $i < count($res); $i++) { 
    foreach ($res[$i] as $key => $value) {
    }

  //BUSCAR O NOME DO FUNCIONÁRIO

  $descricao = $res[$i]['descricao'];
  $funcionario = $res[$i]['funcionario'];

	if($descricao == 'Compra de Remédios'){
		$res_excluir = $pdo->query("select * from funcionarios where cpf = '$funcionario'");
	}else{
		$res_excluir = $pdo->query("select * from tesoureiros where cpf = '$funcionario'");
	}
	
	$dados_excluir = $res_excluir->fetchAll(PDO::FETCH_ASSOC);
	$nome_func = $dados_excluir[0]['nome'];

  

   $dados[] = array(
     'id' => $res[$i]['id'],
     'descricao' => $res[$i]['descricao'],
     'valor' => $res[$i]['valor'],
     
     'pago' => $res[$i]['pago'],
     'funcionario' => $nome_func,
     
          
      
   );

}

      if(count($res) > 0){
              $result = json_encode(array('success'=>true, 'result'=>$dados));

          }else{
              $result = json_encode(array('success'=>false, 'result'=>'0'));

          }
          echo $result;

}




else if($postjson['requisicao'] == 'add'){

  //BUSCAR O CPF DO USUÁRIO LOGADO (NESSE CASO UM TESOUREIRO)
$res_excluir = $pdo->query("select * from tesoureiros where email = '$postjson[usuario]' ");
$dados_excluir = $res_excluir->fetchAll(PDO::FETCH_ASSOC);
$func= $dados_excluir[0]['cpf'];


  $query = $pdo->prepare("INSERT INTO contas_pagar SET descricao = :descricao, valor = :valor, vencimento = :vencimento, pago = :pago, funcionario = :funcionario, foto = :foto ");

     $query->bindValue(":descricao", $postjson['descricao']);
     $query->bindValue(":valor", $postjson['valor']);
     $query->bindValue(":vencimento", $postjson['vencimento']);
     $query->bindValue(":pago", 'Não');
     $query->bindValue(":funcionario", $func);
     $query->bindValue(":foto", 'sem-foto.png');
     $query->execute();

     $id = $pdo->lastInsertId();
     

    if($query){
      $result = json_encode(array('success'=>true, 'id'=>$id));

      }else{
      $result = json_encode(array('success'=>false));
  
      }
   echo $result;


    }


    elseif($postjson['requisicao'] == 'excluir'){
    
            
      $query = $pdo->query("DELETE FROM contas_pagar where id = '$postjson[id]'");
    
               
    
        if($query){
          $result = json_encode(array('success'=>true));
    
          }else{
          $result = json_encode(array('success'=>false));
      
          }
       echo $result;
  
      }



      elseif($postjson['requisicao'] == 'baixar'){
    
            
        $query = $pdo->query("UPDATE contas_pagar SET pago = 'Sim', pagamento = curDate() where id = '$postjson[id]'");
      
                 
      
          if($query){
            $result = json_encode(array('success'=>true));
      
            }else{
            $result = json_encode(array('success'=>false));
        
            }
         echo $result;
    
        }



        else if($postjson['requisicao'] == 'movimentacoes'){

          $dataInicial = $postjson['dataInicial'];
          $dataFinal = $postjson['dataFinal'];

          if($dataInicial == ""){
            $dataInicial = date('Y-m-d');
          }

          if($dataFinal == ""){
            $dataFinal = date('Y-m-d');
          }

         
          $query = $pdo->query("SELECT * from movimentacoes where data >= '$dataInicial' and data <= '$dataFinal' order by id desc limit $postjson[start], $postjson[limit]");
         
        
        
          $res = $query->fetchAll(PDO::FETCH_ASSOC);

          $total_entradas = 0;
          $total_saidas = 0;
        
         for ($i=0; $i < count($res); $i++) { 
            foreach ($res[$i] as $key => $value) {
            }


           
            $tipo = $res[$i]['tipo'];
            $valor = $res[$i]['valor'];
            
          
            if($tipo == 'Entrada'){
              @$total_entradas = $total_entradas + $valor;
            }else{
              @$total_saidas = $total_saidas + $valor;
            }

          

         
            
          $data2 = implode('/', array_reverse(explode('-', $res[$i]['data'])));
              
           $dados[] = array(
             'id' => $res[$i]['id'],
             'tipo' => $res[$i]['tipo'],
             'movimento' => $res[$i]['movimento'],
             
             'valor' => $res[$i]['valor'],
             'funcionario' => $res[$i]['tesoureiro'],
             'data2' => $data2,
                      
                  
              
           );
        
          }

          @$total = @$total_entradas - @$total_saidas;
        
              if(count($res) > 0){
                      $result = json_encode(array('success'=>true, 'result'=>$dados, 'total'=>$total, 'total_entradas'=>$total_entradas, 'total_saidas'=>$total_saidas));
        
                  }else{
                      $result = json_encode(array('success'=>false, 'result'=>'0'));
        
                  }
                  echo $result;
        
        }
        

    


?>